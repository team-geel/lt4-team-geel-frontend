export function isObject ( value: any )
{
  return ( value !== null && value !== undefined && !Array.isArray( value ) && ( typeof value === 'object' || typeof value === 'function' ) );
}

export function hasValues ( obj: object )
{
  return Object.values( obj ).some( item => item !== null && typeof item !== "undefined" )
}

export function validateInput ( input: string )
{
  const regexp = new RegExp( /[\t\r\n]|(--[^\r\n]*)|(\/\*[\w\W]*?(?=\*)\*\/)/gi );

  if ( regexp.test( input ) )
  {
    alert( "Input is not valid" );
    //Todo: better error handling
    return input.replace( regexp, "" );
  }

  return input;
}

export function getDateNow ( withSeconds: boolean = false )
{
  const locale: string = "nl-NL";
  let displayOptions: Intl.DateTimeFormatOptions = {
    weekday: "long",
    day: "numeric",
    month: "short",
    hour: "numeric",
    minute: "numeric",
  }

  if ( withSeconds )
  {
    displayOptions = {
      ...displayOptions,
      second: "numeric",
    }
  }

  return new Date().toLocaleString( locale, displayOptions );
}

export function calcDateDifference ( refferenceDate: Date, dateToCheck: Date )
{
  dateToCheck.setHours( dateToCheck.getHours() + 1 );
  return new Date( refferenceDate.getTime() - dateToCheck.getTime() );
}

export function timestampToDate ( timestamp: string )
{
  const time = timestamp.split( ":" );
  const date = new Date();
  return new Date( date.setHours( +time[ 0 ], +time[ 1 ], +time[ 2 ] ) );
}

export function roundToTwoDecimals ( num: number )
{
  return Math.round( ( num + Number.EPSILON ) * 100 ) / 100;
}
