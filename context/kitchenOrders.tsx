import { getOrderedOrders } from "@/fetch/orders";
import { KitchenOrdersContext, OrderedItemType } from "@/types/KitchenOrderTypes";
import { ReactNode, useEffect, useState } from "react";
import { createCtx } from "./createContext";

const updateTime = 5000; // milliseconds
const initialOrders: OrderedItemType[] = [];

export function KitchenProvider ( { children }: { children: ReactNode } )
{
  const [ kitchenOrders, setkitchenOrders ] =
    useState<OrderedItemType[]>( initialOrders );
  const [ allOrders, setAllOrders ] = useState<OrderedItemType[]>( initialOrders );

  useEffect( () =>
  {
    function filterOrders ( orders: OrderedItemType[] )
    {
      return orders.filter( ( order ) =>
      {
        return order.status === "In voorbereiding";
      } );
    }

    const fetchData = async () =>
    {
      const result = await getOrderedOrders();
      setkitchenOrders( filterOrders( result ) );
    }

    fetchData() //to make sure on load of page the data is immediately loaded
    const interval = setInterval( () =>
    {
      fetchData()
    }, updateTime );

    return () => clearInterval( interval );
  }, [] )

  return (
    <KitchenOrdersProvider value={ { kitchenOrders, setkitchenOrders } }>
      { children }
    </KitchenOrdersProvider>
  );
}

const [ useKitchenOrdersContext, KitchenOrdersProvider ] =
  createCtx<KitchenOrdersContext>( {} as KitchenOrdersContext );

export function useKitchenOrders ()
{
  return useKitchenOrdersContext();
}
