import { createCtx } from "./createContext";
import { useLocalStorage } from "@/hooks/useLocalStorage";
import { ReactNode } from "react";
import { AuthContext, AuthType, Roles } from "@/types/UserTypes";
import router from "next/router";

export function AuthContextProvider ( { children }: { children: ReactNode } )
{
  const [ auth, setAuth ] = useLocalStorage<AuthType>( "auth", {} as AuthType );

  function isAdmin ()
  {
    return Object.values( Roles.ADMIN ).includes( auth.role );
  }

  function isLoggedIn ()
  {
    return typeof auth != "undefined" && Object.values( Roles ).includes( auth.role );
  }

  function logout ()
  {
    setAuth( {} as AuthType );
    router.push( "/login" );
  }

  return (
    <AuthProvider value={ { auth, isAdmin, isLoggedIn, logout, setAuth } }>{ children }</AuthProvider>
  );
}

const [ useAuthContext, AuthProvider ] =
  createCtx<AuthContext>( {} as AuthContext );

export function useAuth ()
{
  return useAuthContext();
}
