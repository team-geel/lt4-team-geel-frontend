import { useLocalStorage } from "@/hooks/useLocalStorage";
import { SelectedConsumablesContext, SelectedConsumableItemType, ConsumableItemType } from "@/types/ConsumableTypes";
import { ReactNode } from "react";
import { createCtx } from "./createContext";

const [ useSelectedConsumablesContext, SelectedConsumablesProvider ] = createCtx<SelectedConsumablesContext>( {} as SelectedConsumablesContext );

export const initialselectedConsumables: SelectedConsumableItemType[] = [];

export function SelectedConsumablesContextProvider ( { children }: { children: ReactNode } )
{
  const [ selectedConsumables, setSelectedConsumables ] = useLocalStorage<SelectedConsumableItemType[]>( 'selectedConsumables', initialselectedConsumables as SelectedConsumableItemType[] );

  /**
   *
   * @param id
    * @returns number
   */
  function getItemQuantity ( consumable: ConsumableItemType ): number
  {
    return selectedConsumables.find( ( selectedConsumable: SelectedConsumableItemType ) => selectedConsumable.id === consumable.id )?.quantity || 0;
  }

  /**
   *
   * @param id
   * @return void
   */
  function increaseItemQuantity ( consumable: ConsumableItemType )
  {
    setSelectedConsumables( currentSelectedConsumables =>
    {
      //Try to find item in current list and if not found, add new item to list
      if ( currentSelectedConsumables.find( item => item.id === consumable.id ) == null )
      {
        return [ ...currentSelectedConsumables, { ...consumable, quantity: 1 } ];
      }

      //Go over all items and update the quantity of that item by 1
      return currentSelectedConsumables.map( selectedConsumable =>
      {
        if ( selectedConsumable.id === consumable.id )
        {
          return { ...selectedConsumable, quantity: selectedConsumable.quantity + 1 };
        }

        return selectedConsumable;
      } )
    } )
  }

  /**
   *
   * @param id
   * @return void
   */
  function decreaseItemQuantity ( consumable: ConsumableItemType )
  {
    setSelectedConsumables( currentSelectedConsumables =>
    {
      //If item found in list has 1 quantity left. Remove this item from the list
      if ( currentSelectedConsumables.find( selectedConsumable => selectedConsumable.id === consumable.id )?.quantity === 1 )
      {
        return currentSelectedConsumables.filter( selectedConsumable => selectedConsumable.id !== consumable.id );
      }

      //Go over all items and update the quantity of that item by 1
      return currentSelectedConsumables.map( selectedConsumable =>
      {
        if ( selectedConsumable.id === consumable.id )
        {
          return { ...selectedConsumable, quantity: selectedConsumable.quantity - 1 };
        }

        return selectedConsumable;
      } )
    } )
  }

  /**
   *
   * @param id
   * @return void
   */
  function removeItem ( consumable: ConsumableItemType )
  {
    setSelectedConsumables( currentSelectedConsumables =>
    {
      return currentSelectedConsumables.filter( selectedConsumable => selectedConsumable.id !== consumable.id );
    } )
  }

  return (
    <SelectedConsumablesProvider value={ { getItemQuantity, increaseItemQuantity, decreaseItemQuantity, removeItem, selectedConsumables, setSelectedConsumables } }>
      { children }
    </SelectedConsumablesProvider>
  );
}

export function useSelectedConsumables ()
{
  return useSelectedConsumablesContext();
}
