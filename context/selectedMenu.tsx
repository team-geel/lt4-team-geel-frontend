import { useLocalStorage } from "@/hooks/useLocalStorage";
import { SelectedMenu, SelectedMenuContext } from "@/types/SelectedMenuTypes";
import { ReactNode } from "react";
import { createCtx } from "./createContext";

export const selectAllCategoryItem = "Alles";
const initialSelectedMenu: SelectedMenu = {
  filterType: undefined,
  filterCategory: selectAllCategoryItem,
}

export function ConsumableMenuProvider ( { children }: { children: ReactNode } )
{
  const [ selectedMenu, setSelectedMenu ] = useLocalStorage<SelectedMenu>( 'selectedMenu', initialSelectedMenu );

  return (
    <SelectedMenuProvider value={ { selectedMenu, setSelectedMenu } }>
      { children }
    </SelectedMenuProvider>
  );
}

const [ useSelectedMenuContext, SelectedMenuProvider ] = createCtx<SelectedMenuContext>( {} as SelectedMenuContext );

export function useSelectedMenu ()
{
  return useSelectedMenuContext();
}
