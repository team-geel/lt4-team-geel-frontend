import { ReactNode } from "react";
import { useLocalStorage } from "@/hooks/useLocalStorage";
import {
  serviceRequestContext,
  serviceRequestType,
} from "@/types/ServiceRequestTypes";
import { createCtx } from "./createContext";

const [useServiceRequestContext, ServiceRequestProvider] =
  createCtx<serviceRequestContext>({} as serviceRequestContext);
export const initialServiceRequestDetails: serviceRequestType = {
  serviceRequest: "",
};

export function ServiceRequestContextProdiver({
  children,
}: {
  children: ReactNode;
}) {
  const [serviceRequest, setServiceRequest] =
    useLocalStorage<serviceRequestType>(
      "serviceRequest",
      initialServiceRequestDetails as serviceRequestType
    );

  return (
    <ServiceRequestProvider value={{ serviceRequest, setServiceRequest }}>
      {children}
    </ServiceRequestProvider>
  );
}

export function useServiceRequestDetails() {
  return useServiceRequestContext();
}
