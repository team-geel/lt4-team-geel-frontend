import { useLocalStorage } from "@/hooks/useLocalStorage";
import { OrderDetailsContext, OrderDetailsType } from "@/types/OrderTypes";
import { ReactNode } from "react";
import { createCtx } from "./createContext";

const [useOrderDetailsContext, OrderDetailsProvider] =
  createCtx<OrderDetailsContext>({} as OrderDetailsContext);

export const initialOrderDetails: OrderDetailsType = {
  tableNumber: 0,
  orderComment: "",
};

export function OrderContextProvider({ children }: { children: ReactNode }) {
  const [orderDetails, setOrderDetails] = useLocalStorage<OrderDetailsType>(
    "orderDetails",
    initialOrderDetails as OrderDetailsType
  );

  return (
    <OrderDetailsProvider value={{ orderDetails, setOrderDetails }}>
      {children}
    </OrderDetailsProvider>
  );
}

export function useOrderDetails() {
  return useOrderDetailsContext();
}
