import { useContext, createContext } from "react";

export function createCtx<ContextType> ( initialValue?: ContextType )
{
  const contextProvider = createContext( initialValue as ContextType );

  function useProvider ()
  {
    const context = useContext( contextProvider );
    if ( !context )
      throw new Error(
        "useProvider must be inside a Provider with a value"
      );
    return context;
  }
  return [ useProvider, contextProvider.Provider ] as const;

}
