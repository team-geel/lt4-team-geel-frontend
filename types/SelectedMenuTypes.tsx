import { Dispatch, ReactNode, SetStateAction } from "react";

export enum MenuTypes
{
  MEALS = "food",
  DRINKS = "drink"
}

export interface SelectedMenu
{
  filterType?: MenuTypes;
  filterCategory?: string;
}

export interface SelectedMenuContext
{
  selectedMenu: SelectedMenu;
  setSelectedMenu: Dispatch<SetStateAction<SelectedMenu>>;
}

export interface ConsumableNavigationProps
{
  children?: ReactNode;
  categories: string[];
}
