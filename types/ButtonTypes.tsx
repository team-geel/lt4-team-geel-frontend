import { ReactNode } from "react";
import { CustomComponent } from "./ComponentTypes";
import { MenuTypes } from "./SelectedMenuTypes";

export enum ButtonTypes
{
  DEFAULT = "default",
  DARK = "dark",
  LIGHT = "light",
  PRIMARY = "primary",
  SECONDARY = "secondary",
  ACTION = "action",
  LINE = "line",
  TEXT = "text",
  TRANSPARENT = "transparent",
}

export enum ButtonSizes
{
  SMALL = "small",
  REGILAR = "regular",
  BIG = "big",
}

export interface ConsumablePageLinkButtonProps extends PageLinkProps
{
  menuType: MenuTypes;
}

export interface PageLinkProps extends ActionButtonProps
{
  children: ReactNode;
  href: string;
  alt: string;
  imageSrc?: string;
}

export interface ActionButtonProps extends ButtonProps
{
  onClick?: any;
  href?: string;
}

export interface ButtonProps extends CustomComponent
{
  type?: "button" | "reset" | "submit";
  styleType?: ButtonTypes;
  buttonSize?: ButtonSizes;
  disabled?: boolean;
  active?: boolean;
}
