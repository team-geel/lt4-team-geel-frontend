import { ChangeEventHandler, FormEventHandler, ReactNode } from "react";

export interface FormProps
{
  children: ReactNode;
  onSubmit: FormEventHandler<HTMLFormElement>
}

export enum InputTypes
{
  BUTTON = 'button',
  CHECKBOX = 'checkbox',
  COLOR = 'color',
  DATE = 'date',
  DATETIMELOCAL = 'datetimelocal',
  EMAIL = 'email',
  FILE = 'file',
  HIDDEN = 'hidden',
  IMAGE = 'image',
  MONTH = 'month',
  NUMBER = 'number',
  PASSWORD = 'password',
  RADIO = 'radio',
  RANGE = 'range',
  RESET = 'reset',
  SEARCH = 'search',
  SUBMIT = 'submit',
  TEL = 'tel',
  TEXT = 'text',
  TIME = 'time',
  URL = 'url',
  WEEK = 'week',
}

export interface SelectProps
{
  name: string;
  currentValue: string;
  options: string[];
  onChange?: ChangeEventHandler<HTMLSelectElement>;
}

export interface InputProps
{
  type: InputTypes;
  name: string;
  step?: string;
  accept?: string;
  value: string | number;
  placeholder: string;
  onChange?: ChangeEventHandler<HTMLInputElement>
}

export interface FormGroupProps
{
  children: ReactNode;
  name: string;
  labelText: string;
}
