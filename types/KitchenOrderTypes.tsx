import { Dispatch, SetStateAction } from "react";
import { OrderConsumableItemType, OrderType } from "./OrderTypes";

export interface OrderedItemsProps extends OrderedItemType {}

export interface OrderedItemProps extends OrderConsumableItemType {}

export interface OrderedItemType extends OrderType {
  id: number;
  timestamp: string;
  status: string;
}

export interface KitchenOrdersContext {
  kitchenOrders: OrderedItemType[];
  setkitchenOrders: Dispatch<SetStateAction<OrderedItemType[]>>;
}
