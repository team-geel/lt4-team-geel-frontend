import { Dispatch, SetStateAction } from "react";

export interface serviceRequestType {
  serviceRequest: string;
}

export interface serviceRequestContext {
  serviceRequest: serviceRequestType;
  setServiceRequest: Dispatch<SetStateAction<serviceRequestType>>;
}
