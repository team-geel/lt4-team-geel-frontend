import { JSXElementConstructor, PropsWithChildren, ReactElement, ReactNode } from "react";
import { ConsumableItemType, SelectedConsumableItemType } from "./ConsumableTypes";

export interface CustomComponent extends PropsWithChildren
{
  accesskey?: string;
  className?: string;
  hidden?: boolean;
  id?: string;
  tabindex?: number;
  title?: string;
}

export interface ImageSliderProps
{
  images?: string[];
  properties?: {
    duration?: number;
    transitionDuration?: number;
    defaultIndex?: number;
    indicators?: boolean | ( ( index?: number ) => ReactNode ),
    prevArrow?: ReactElement<any, string | JSXElementConstructor<any>>,
    nextArrow?: ReactElement<any, string | JSXElementConstructor<any>>;
    arrows?: boolean;
    autoplay?: boolean;
    infinite?: boolean;
    pauseOnHover?: boolean;
    canSwipe?: boolean;
    easing?: string;
  }
}

export interface ConsumableCounterProps
{
  consumable: ConsumableItemType | SelectedConsumableItemType
}
