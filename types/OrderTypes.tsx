import { Dispatch, SetStateAction } from "react";

export interface OrderDetailsType
{
  tableNumber: number;
  orderComment: string;
}

export interface OrderDetailsContext
{
  orderDetails: OrderDetailsType;
  setOrderDetails: Dispatch<SetStateAction<OrderDetailsType>>;
}

//Format from database. This should later be replaced with only id and quantity or SelectedConsumableItemType
export interface OrderConsumableItemType
{
  name: string;
  price: number;
  quantity: number;
}

export interface OrderType extends OrderDetailsType
{
  orderList: OrderConsumableItemType[];
}

export interface UpdateOrderType
{
  status: string;
}
