import { Dispatch, SetStateAction } from "react";
import { MenuTypes } from "./SelectedMenuTypes";

export interface NewConsumableItemType
{
  name: string;
  price: number;
  image: string;
  type: string;
  category: string;
  description: string;
}

export interface ConsumableItemType extends NewConsumableItemType
{
  id: number;
}

export interface ConsumableItemProps
{
  consumable: ConsumableItemType | SelectedConsumableItemType;
  deleteItem?: boolean;
}
export interface NewConsumableType
{
  name: string;
  price: number;
  image: string;
  type: string;
  category: string;
  description: string;
}
export interface ConsumableProps
{
  forType: MenuTypes;
}

export interface SelectedConsumableItemType extends ConsumableItemType
{
  quantity: number;
}

export interface SelectedConsumablesContext
{
  getItemQuantity: ( consumable: ConsumableItemType ) => number;
  increaseItemQuantity: ( consumable: ConsumableItemType ) => void;
  decreaseItemQuantity: ( consumable: ConsumableItemType ) => void;
  removeItem: ( consumable: ConsumableItemType ) => void;
  selectedConsumables: SelectedConsumableItemType[];
  setSelectedConsumables: Dispatch<
    SetStateAction<SelectedConsumableItemType[]>
  >;
}

export interface ConsumableFormProps
{
  consumable?: ConsumableItemType;
}
