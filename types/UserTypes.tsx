import { Dispatch, SetStateAction } from "react";

export enum Roles
{
  ADMIN = "ADMIN",
  WAITER = "WAITER",
  CHEF = "CHEF",
}

export interface UserLoginType
{
  email: string;
  password: string;
}

interface UserDetails
{
  firstname: string;
  lastname: string;
  email: string;
  role: Roles;
}

export interface UserFormProps
{
  user?: UserType;
}

export interface NewUserType extends UserDetails
{
  password: string;
}

export interface UserType extends UserDetails
{
  id: number;
}

export interface AuthType extends UserDetails
{
  token: string;
}

export interface AuthContext
{
  auth: AuthType;
  isAdmin (): boolean;
  isLoggedIn (): boolean;
  logout (): void;
  setAuth: Dispatch<SetStateAction<AuthType>>;
}
