/// <reference types="cypress" />
import "@/styles/globals.css";
import DateTimeCounter from "@/components/counters/DateTimeCounter";
import { getDateNow } from "@/utils/globalFunctions";

describe('<DateTimeCounter>', () => {
  it("mounts", () => {
    cy.mount(
    <DateTimeCounter />
    )
  })

  it("shows the date and time at this moment", () => {
    cy.get('span').should('have.text', getDateNow())
  })
})

export {}
