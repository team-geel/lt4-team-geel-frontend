describe('Navigation.cy.ts', () => {
  it('should navigate to the ask page', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/')

    // Find a link with an href attribute containing "ask" and click it
    cy.get('a[href*="ask"]').click()

    // The new url should include "/ask"
    cy.url().should('include', '/ask')

    cy.get('p').contains('We zullen u zo snel mogelijk helpen, duurt het lang? Vraag gerust een personeelslid om hulp.')
  })
})

export {}
