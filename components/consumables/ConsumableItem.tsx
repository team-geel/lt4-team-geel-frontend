import Image from 'next/image';
import styles from '@/styles/ConsumableItem.module.css';
import { ConsumableItemProps } from '@/types/ConsumableTypes';
import { useSelectedConsumables } from '@/context/selectedConsumables';
import ActionButton from '../buttons/ActionButton';
import { ButtonTypes } from '@/types/ButtonTypes';
import ConsumableCounter from '../counters/ConsumableCounter';
import { useEffect, useState } from 'react';
import { roundToTwoDecimals } from '@/utils/globalFunctions';

const ConsumableItem = ( { consumable, deleteItem = false }: ConsumableItemProps ) =>
{
  const { getItemQuantity, increaseItemQuantity, removeItem } = useSelectedConsumables();
  const [ totalPrice, setTotalPrice ] = useState( consumable.price );
  const selectableClass = ( getItemQuantity( consumable ) === 0 ? styles.selectable : "" );

  function handleFirstSelection ()
  {
    if ( getItemQuantity( consumable ) === 0 )
    {
      increaseItemQuantity( consumable );
    }
  }

  function removeConsumable ()
  {
    removeItem( consumable );
  }


  useEffect( () =>
  {
    if ( 'quiantity' in consumable )
    {
      setTotalPrice( roundToTwoDecimals( getItemQuantity( consumable ) * consumable.price ) );
    }
  }, [ getItemQuantity ] )

  return (
    <>
      <div className={ `${ styles.consumable } ${ selectableClass }` }>
        <a className={ styles.consumable__link } onClick={ handleFirstSelection }>
          <div className={ styles.consumable__data }>
            <div className={ styles.colum }>
              <Image
                src={ consumable.image }
                alt={ consumable.name + " image" }
                className={ styles.consumable__image }
                width={ 80 }
                height={ 80 }
              />
            </div>
            <div className={ styles.colum }>
              <label className={ styles.consumable__name }>{ consumable.name }</label>
            </div>
            <div className={ styles.colum }>
              <label className={ styles.consumable__price }>
                <span>€</span> <span>{ totalPrice.toFixed( 2 ) }</span>
              </label>
              { getItemQuantity( consumable ) >= 1 && <ConsumableCounter consumable={ consumable } /> }
            </div>.
          </div>
        </a>
        { deleteItem &&
          <div className={ styles.consumable__remove }>
            <ActionButton styleType={ ButtonTypes.LINE } onClick={ removeConsumable }>
              Verwijder
            </ActionButton>
          </div>
        }
      </div>
    </>

  );
};

export default ConsumableItem;
