import { Suspense, useEffect, useState } from "react";
import styles from "@/styles/Orders.module.css";
import
{
  ConsumableItemType,
  SelectedConsumableItemType,
} from "@/types/ConsumableTypes";
import Loading from "../Loading";
import
{
  initialselectedConsumables,
  useSelectedConsumables,
} from "@/context/selectedConsumables";
import { initialOrderDetails, useOrderDetails } from "@/context/orders";
import { getConsumables } from "@/fetch/consumables";
import { ButtonTypes } from "@/types/ButtonTypes";
import ActionButton from "../buttons/ActionButton";
import ConsumableItem from "./ConsumableItem";
import { OrderConsumableItemType, OrderType } from "@/types/OrderTypes";
import { roundToTwoDecimals } from "@/utils/globalFunctions";
import { postOrder } from "@/fetch/orders";
import TablenumberPicker from "../pickers/TableNumberPicker";

const SelectedConsumables = () =>
{
  const initialConsumables: SelectedConsumableItemType[] = [];
  const [ consumables, setConsumables ] =
    useState<SelectedConsumableItemType[]>( initialConsumables );
  const [ totalPrice, setTotalPrice ] = useState( 0 );

  const { orderDetails, setOrderDetails } = useOrderDetails();
  const { selectedConsumables, setSelectedConsumables } =
    useSelectedConsumables();

  function confirmOrder ()
  {
    if ( confirm( "Bevestigen van order?" ) )
    {
      const orderList: OrderConsumableItemType[] = consumables.map(
        ( consumable ) => ( {
          name: consumable.name,
          quantity: consumable.quantity,
          price: consumable.price,
        } )
      );

      const order: OrderType = {
        tableNumber: orderDetails.tableNumber,
        orderComment: orderDetails.orderComment,
        orderList: orderList,
      };

      if ( postOrder( order ) )
      {
        setSelectedConsumables( initialselectedConsumables );
        setOrderDetails( {
          tableNumber: orderDetails.tableNumber,
          orderComment: "",
        } );
      }
    }
  }

  function filteredConsumables (
    consumables: ConsumableItemType[]
  ): SelectedConsumableItemType[]
  {
    return selectedConsumables.map( ( selectedConsumable ) => ( {
      ...selectedConsumable,
      ...consumables.find(
        ( consumable ) => consumable.id === selectedConsumable.id
      ),
    } ) );
  }

  useEffect( () =>
  {
    setTotalPrice(
      roundToTwoDecimals(
        consumables.reduce( ( sum, currentItem ) =>
        {
          return sum + currentItem.price * currentItem.quantity;
        }, 0 )
      )
    );
  }, [ consumables.length, selectedConsumables ] );

  useEffect( () =>
  {
    const fetchData = async () =>
    {
      const result = await getConsumables();
      setConsumables( filteredConsumables( result ) );
    }

    fetchData();

  }, [ selectedConsumables, orderDetails ] );

  return (
    <>
      { consumables.length > 0 ? (
        orderDetails.tableNumber !== 0 ? (
          <div className={ styles.confirm }>
            <ActionButton
              styleType={ ButtonTypes.PRIMARY }
              onClick={ confirmOrder }
            >
              Bevestigen
            </ActionButton>
          </div>
        ) : (
          <>
            <p>Voer eerst een geldig tafelnummer in</p>
            <TablenumberPicker />
          </>
        )
      ) : (
        <p>Kies eerst een gerecht of drankje</p>
      ) }

      <div className={ styles.totalPrice }>Totaal € { totalPrice.toFixed( 2 ) }</div>
      <Suspense fallback={ undefined }>
        <div className={ styles.container }>
          <div className={ styles.orders }>
            { consumables && consumables.length > 0 ? (
              filteredConsumables( consumables ).map(
                ( consumable: SelectedConsumableItemType, index: number ) =>
                {
                  return (
                    <ConsumableItem
                      key={ `OrderItem ${ index }` }
                      deleteItem={ true }
                      consumable={ consumable }
                    />
                  );
                }
              )
            ) : (
              <Loading />
            ) }
          </div>
        </div>
      </Suspense>
    </>
  );
};

export default SelectedConsumables;
