import ConsumableItem from "@/components/consumables/ConsumableItem";
import Loading from "@/components/Loading";
import ConsumableNavigation from "@/components/navigation/ConsumableNavigation";
import { selectAllCategoryItem, useSelectedMenu } from "@/context/selectedMenu";
import { getConsumables } from "@/fetch/consumables";
import styles from '@/styles/Consumables.module.css';
import { ConsumableItemType, ConsumableProps } from "@/types/ConsumableTypes";
import { Suspense, useEffect, useState } from "react";
import NavigationButtons from "../navigation/NavigationButtons";

const Consumables = ( { forType }: ConsumableProps ) =>
{
  const initialConsumables: ConsumableItemType[] = [];
  const { selectedMenu, setSelectedMenu } = useSelectedMenu();
  const [ consumables, setconsumables ] = useState<ConsumableItemType[]>( initialConsumables );


  //Make sure that if url is typed by user, the selected menu will be selected correctly
  if ( selectedMenu.filterType !== forType )
  {
    setSelectedMenu( { ...selectedMenu, filterType: forType, filterCategory: selectAllCategoryItem } );
  }

  function filterConsumables ( consumables: ConsumableItemType[] )
  {
    if ( !selectedMenu?.filterType )
    {
      return consumables;
    }

    return consumables.filter( ( consumable: ConsumableItemType ) =>
    {
      return ( selectedMenu.filterType === consumable.type ) && ( !selectedMenu.filterCategory || selectedMenu.filterCategory === consumable.category || selectedMenu.filterCategory === selectAllCategoryItem )
    } )
  }

  useEffect( () =>
  {
    const fetchData = async () =>
    {
      const result = await getConsumables();
      setconsumables( result );
    }

    fetchData();

  }, [] )

  function getCategories ()
  {
    return Array.from( new Set( [
      selectAllCategoryItem, //add default category to array
      ...consumables.filter( ( consumable: ConsumableItemType ) => selectedMenu.filterType === consumable.type && consumable.category
      ).map( ( consumable: ConsumableItemType ) => consumable.category )
    ] ) )
  }

  if ( !consumables )
  {
    return <p>Geen items gevonden</p>;
  };

  if ( consumables.length <= 0 )
  {
    return <Loading />;
  }

  return ( <>
    <Suspense fallback={ undefined }>
      <ConsumableNavigation categories={ getCategories() } />
      <div className={ styles.container } >
        <div className={ styles.consumables }>
          {
            filterConsumables( consumables ).map( ( consumable: ConsumableItemType, index: number ) =>
            {
              return <ConsumableItem key={ "Consumable " + index } consumable={ consumable } />;
            } )
          }
        </div>
      </div>
    </Suspense>
    <NavigationButtons />
  </> );
}

export default Consumables;
