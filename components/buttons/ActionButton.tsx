import { ActionButtonProps, ButtonSizes, ButtonTypes } from "@/types/ButtonTypes";
import Button from './Button';
import styles from '@/styles/Buttons.module.css';
import { forwardRef } from "react";

type Ref = HTMLAnchorElement;

const ActionButton = forwardRef<Ref, ActionButtonProps>( ( {
  children,
  className = "",
  onClick,
  href,
  type = "button",
  styleType = ButtonTypes.DEFAULT,
  buttonSize = ButtonSizes.REGILAR,
  disabled = false,
  active = false,
}, ref ) =>
{

  return (
    <>
      <a href={ href } onClick={ onClick } ref={ ref } tabIndex={ -1 } className={ styles.actionLink }>
        <Button
          type={ type }
          styleType={ styleType }
          buttonSize={ buttonSize }
          disabled={ disabled }
          active={ active }
          className={ `${ className }` }>
          { children }
        </Button>
      </a>
    </>
  )
} );

ActionButton.displayName = "ActionButton";

export default ActionButton;
