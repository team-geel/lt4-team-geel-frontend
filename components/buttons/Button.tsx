import styles from '@/styles/Buttons.module.css';
import { ButtonProps, ButtonSizes, ButtonTypes } from "@/types/ButtonTypes";

const Button = ( {
  children,
  className = "",
  type = "button",
  styleType = ButtonTypes.DEFAULT,
  buttonSize = ButtonSizes.REGILAR,
  disabled = false,
  active = false,
}: ButtonProps ) =>
{
  const disabledClass = ( disabled ? styles.disabled : "" );
  const activeClass = ( active ? styles.active : "" );

  return (
    <button
      type={ type }
      className={ `${ styles.button } ${ styles[ styleType ] } ${ styles[ buttonSize ] } ${ className } ${ disabledClass } ${ activeClass }` }>
      { children }
    </button>
  )
}

export default Button;
