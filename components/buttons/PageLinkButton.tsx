import { ButtonTypes, PageLinkProps } from "@/types/ButtonTypes";
import Link from "next/link";
import Image from "next/image";
import ActionButton from "./ActionButton";

const PageLinkButton = ( {
  children,
  href,
  alt,
  onClick,
  styleType = ButtonTypes.LINE,
  buttonSize,
  disabled,
  active,
  imageSrc
}: PageLinkProps ) =>
{
  return (
    <>
      <Link
        href={ href }
        passHref
        legacyBehavior
      >
        <ActionButton styleType={ styleType } buttonSize={ buttonSize } disabled={ disabled } active={ active } onClick={ onClick }>
          { imageSrc &&
            <Image
              src={ imageSrc }
              alt={ alt }
              width='50'
              height='50'
            />
          }
          { children }
        </ActionButton>
      </Link>
    </>
  );
}

export default PageLinkButton;
