import { ButtonTypes, ConsumablePageLinkButtonProps } from "@/types/ButtonTypes";
import { MenuTypes } from "@/types/SelectedMenuTypes";
import Link from "next/link";
import Image from "next/image";
import ActionButton from "./ActionButton";
import { selectAllCategoryItem, useSelectedMenu } from "@/context/selectedMenu";
import PageLinkButton from "./PageLinkButton";

const ConsumablePageLinkButton = ( {
  children,
  href,
  alt,
  styleType = ButtonTypes.LINE,
  buttonSize,
  disabled,
  active,
  menuType,
  imageSrc
}: ConsumablePageLinkButtonProps ) =>
{
  const { selectedMenu, setSelectedMenu } = useSelectedMenu();

  function changeMenuType ( type: MenuTypes )
  {
    setSelectedMenu( { ...selectedMenu, filterType: type, filterCategory: selectAllCategoryItem } );
  }

  return (
    <>
      <PageLinkButton
        href={ href } alt={ alt }
        styleType={ styleType } buttonSize={ buttonSize }
        onClick={ () => changeMenuType( menuType ) }
        disabled={ disabled } active={ active }
        imageSrc={ imageSrc }
      >
        { children }
      </PageLinkButton>
    </>
  );
}

export default ConsumablePageLinkButton;
