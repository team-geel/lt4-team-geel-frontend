import styles from '@/styles/Forms.module.css';
import { InputProps } from '@/types/FormTypes';

const Input = ( { placeholder, type, name, value, step, accept, onChange }: InputProps ) =>
{
  return (
    <input
      placeholder={ placeholder }
      type={ type }
      name={ name }
      step={ step }
      accept={ accept }
      className={ styles.input }
      value={ value }
      onChange={ onChange }
    />
  )
}

export default Input;
