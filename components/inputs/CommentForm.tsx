import { useOrderDetails } from "@/context/orders";
import { useServiceRequestDetails } from "@/context/serviceRequest";
import { validateInput } from "@/utils/globalFunctions";
import { ChangeEvent, useEffect, useState } from "react";
import styles from "@/styles/CommentForm.module.css";

const CommentForm = ( {
  commentType,
}: {
  commentType: "orderComment" | "serviceRequest";
} ) =>
{
  const initialPlaceholder = "typ hier een bericht voor ons personeel";
  const { orderDetails, setOrderDetails } = useOrderDetails();
  const { serviceRequest, setServiceRequest } = useServiceRequestDetails();
  const [ placeholder, setplaceholder ] = useState<string>( initialPlaceholder );
  const changeComment = ( event: ChangeEvent<HTMLTextAreaElement> ) =>
  {
    if ( commentType === "orderComment" )
    {
      setOrderDetails( {
        ...orderDetails,
        orderComment: validateInput( event.target.value.trim() ),
      } );
    } else if ( commentType === "serviceRequest" )
    {
      setServiceRequest( {
        ...serviceRequest,
        serviceRequest: validateInput( event.target.value.trim() ),
      } );
    }
  };

  useEffect( () =>
  {
    if ( commentType === "orderComment" )
    {
      setplaceholder(
        orderDetails.orderComment
          ? orderDetails.orderComment
          : initialPlaceholder
      );
    } else if ( commentType === "serviceRequest" )
    {
      setplaceholder(
        serviceRequest.serviceRequest
          ? serviceRequest.serviceRequest
          : initialPlaceholder
      );
    }
  }, [ orderDetails.orderComment, serviceRequest.serviceRequest ] );

  return (
    <>
      <textarea
        className={ styles.textarea }
        placeholder={ placeholder }
        onChange={ ( event ) => changeComment( event ) }
        cols={ 50 }
        autoFocus={ true }
      ></textarea>
    </>
  );
};

export default CommentForm;
