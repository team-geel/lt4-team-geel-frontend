import styles from '@/styles/Forms.module.css';
import { SelectProps } from '@/types/FormTypes';

const Select = ( { name, options, onChange, currentValue }: SelectProps ) =>
{
  return (
    <>
      <select className={ styles.select } name={ name } value={ currentValue } onChange={ onChange }>
        {
          options.map( ( option ) =>
          {
            return (
              <option key={ `Option ${ option }` } value={ option }>{ option }</option>
            )
          } )
        }
      </select>
    </>
  )
}

export default Select;
