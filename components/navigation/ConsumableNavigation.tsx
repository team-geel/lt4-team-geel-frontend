import { useSelectedMenu } from "@/context/selectedMenu";
import styles from '@/styles/ConsumableNavigation.module.css';
import { ButtonTypes } from "@/types/ButtonTypes";
import { ConsumableNavigationProps } from "@/types/SelectedMenuTypes";
import ActionButton from "../buttons/ActionButton";

const ConsumableNavigation = ( { categories }: ConsumableNavigationProps ) =>
{
  const { selectedMenu, setSelectedMenu } = useSelectedMenu();

  function updateFilter ( category: string )
  {
    setSelectedMenu( { ...selectedMenu, filterCategory: category } );
  }

  return (
    <>
      <div className={ styles.categories }>
        { categories.map( ( category: string, index: number ) =>
        {
          return (
            <ActionButton
              key={ `Tab ${ category } ${ index }` }
              styleType={ ButtonTypes.TEXT }
              onClick={ () => updateFilter( category ) }
              disabled={ category === selectedMenu.filterCategory }
              className={ `${ styles.category }` }>
              { category }
            </ActionButton>
          )
        } ) }
      </div>
    </>
  )
}

export default ConsumableNavigation;
