import styles from "@/styles/Navbar.module.css";
import { ButtonTypes } from "@/types/ButtonTypes";
import ActionButton from "../../buttons/ActionButton";
import DateTimeCounter from "../../counters/DateTimeCounter";
import { useAuth } from "@/context/auth";
import { usePathname } from "next/navigation";
import SecondaryNavbar from "./SecondaryNavbar";
import { useKitchenOrders } from "@/context/kitchenOrders";
import HomeLink from "../links/HomeLink";

const DesktopNavbar = () =>
{
  const { auth, logout, isLoggedIn } = useAuth();
  const { kitchenOrders } = useKitchenOrders();
  const pathname = usePathname();

  function getTotalOrders ()
  {
    if ( kitchenOrders && kitchenOrders.length > 0 )
    {
      return kitchenOrders.reduce( ( currentLength, order ) =>
      {
        return currentLength + order.orderList.length;
      }, 0 );
    }

    return 0;
  }

  return (
    <>
      <header className={ styles.pageHeader }>
        {
          isLoggedIn() && (
            <>
              <nav className={ styles.desktopNavbar }>
                <div className={ styles.details }>
                  <p className={ styles.headerDate }>
                    <DateTimeCounter />
                  </p>
                  { pathname === "/kitchen" &&
                    <h4 className={ styles.orderTotal }>
                      Orders totaal: { getTotalOrders() }
                    </h4>
                  }
                </div>
                <h2 className={ styles.bannerText }>Lunchroom Schmaak</h2>

                <ActionButton
                  onClick={ logout }
                  styleType={ ButtonTypes.DARK }
                >
                  Uitloggen
                </ActionButton>

              </nav>
              <SecondaryNavbar />
            </>

          )
        }

      </header>
    </>
  );
};

export default DesktopNavbar;
