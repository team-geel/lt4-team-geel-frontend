import ActionButton from "@/components/buttons/ActionButton";
import { useAuth } from "@/context/auth";
import styles from "@/styles/Navbar.module.css";
import { ButtonTypes } from "@/types/ButtonTypes";
import { useRouter } from "next/navigation";

const SecondaryNavbar = () =>
{
  const { auth, isAdmin } = useAuth();
  const router = useRouter();

  const handleNavigation = ( route: string ) =>
  {
    router.push( route );
  };

  return (
    <nav className={ styles.secondaryNavbar }>
      { ( auth && isAdmin ) && (
        <div className={ styles.navItem }>
          <ActionButton styleType={ ButtonTypes.TEXT }
            onClick={ () => handleNavigation( "/admin" ) }
          >
            Administratie
          </ActionButton>
        </div>
      ) }
      { ( auth ) && (
        <>
          <div className={ styles.navItem }>
            <ActionButton styleType={ ButtonTypes.TEXT }
              onClick={ () => handleNavigation( "/kitchen" ) }
            >
              Keuken
            </ActionButton>
          </div>

          <div className={ styles.navItem }>
            <ActionButton styleType={ ButtonTypes.TEXT }
              onClick={ () => handleNavigation( "/requests" ) }
            >
              Geschiedenis
            </ActionButton>
          </div>
        </>
      ) }
    </nav>
  );
};

export default SecondaryNavbar;
