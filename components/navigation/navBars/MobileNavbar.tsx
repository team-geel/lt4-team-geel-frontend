import Link from "next/link";
import Image from 'next/image';
import { usePathname } from "next/navigation";
import styles from "@/styles/Navbar.module.css";

const MobileNavbar = () =>
{
  const pathname = usePathname();

  return (
    <>
      <header className={ styles.pageHeader }>
        { pathname === "/" && <h2 className={ styles.welcomeMessage }>Welkom bij de lunchroom</h2> }
        <nav className={ styles.mobileNavbar }>
          <Link href="/">
            <Image src="/images/Logo.png" alt="" width="200" height="55" />
          </Link>
        </nav>
      </header>
    </>
  )
}

export default MobileNavbar;
