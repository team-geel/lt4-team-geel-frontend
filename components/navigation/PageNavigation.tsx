import styles from '@/styles/PageNavigation.module.css';
import AskLink from './links/AskLink';
import OrdersLink from './links/OrdersLink';
import KitchenLink from './links/KitchenLink';
import MealsLink from './links/MealsLink';
import DrinksLink from './links/DrinksLink';

const PageNavigation = () =>
{
  return (
    <>
      <div className={ styles.pageNavigationButtons } >
        <MealsLink />
        <DrinksLink />
        <AskLink />
        <OrdersLink />
        {/* <KitchenLink /> */ }
      </div>
    </>
  );
}

export default PageNavigation;
