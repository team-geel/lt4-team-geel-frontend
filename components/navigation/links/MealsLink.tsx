import ConsumablePageLinkButton from "@/components/buttons/ConsumablePageLinkButton";
import { MenuTypes } from "@/types/SelectedMenuTypes";

const MealsLink = () =>
{
  return ( <>
    <ConsumablePageLinkButton href={ "/meals" } imageSrc={ "/images/icons/icon-food.png" } alt={ "navigation-meals" } menuType={ MenuTypes.MEALS }>
      <p>Eten</p>
    </ConsumablePageLinkButton>
  </> )
}

export default MealsLink;
