import PageLinkButton from "@/components/buttons/PageLinkButton";

const KitchenLink = () =>
{
  return ( <>
    <PageLinkButton href={ "/kitchen" } imageSrc={ "/images/icons/icon-kitchen.png" } alt={ "navigation-kitchen" }>
      <p>Keuken</p>
    </PageLinkButton>
  </> )
}

export default KitchenLink;
