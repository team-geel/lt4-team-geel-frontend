import PageLinkButton from "@/components/buttons/PageLinkButton";

const AskLink = () =>
{
  return ( <>
    <PageLinkButton href={ "/ask" } imageSrc={ "/images/icons/icon-question.png" } alt={ "navigation-ask" }>
      <p>Vragen</p>
    </PageLinkButton>
  </> )
}

export default AskLink;
