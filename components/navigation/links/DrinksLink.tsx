import ConsumablePageLinkButton from "@/components/buttons/ConsumablePageLinkButton";
import { MenuTypes } from "@/types/SelectedMenuTypes";

const DrinksLink = () =>
{
  return ( <>
    <ConsumablePageLinkButton href={ "/drinks" } imageSrc={ "/images/icons/icon-drink.png" } alt={ "navigation-drinks" } menuType={ MenuTypes.DRINKS }>
      <p>Drinken</p>
    </ConsumablePageLinkButton>
  </> )
}

export default DrinksLink;
