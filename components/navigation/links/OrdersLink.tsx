import PageLinkButton from "@/components/buttons/PageLinkButton";

const OrdersLink = () =>
{
  return ( <>
    <PageLinkButton href={ "/orders" } imageSrc={ "/images/icons/icon-order.png" } alt={ "navigation-orders" }>
      <p>Bestelling</p>
    </PageLinkButton>
  </> )
}

export default OrdersLink;
