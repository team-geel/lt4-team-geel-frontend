import PageLinkButton from "@/components/buttons/PageLinkButton";

const HomeLink = () =>
{
  return ( <>
    <PageLinkButton href={ "/" } imageSrc={ "/images/icons/icon-home.png" } alt={ "navigation-home" }>
      <p>Home</p>
    </PageLinkButton>
  </> )
}

export default HomeLink;
