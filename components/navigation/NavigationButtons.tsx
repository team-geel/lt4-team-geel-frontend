import styles from '@/styles/NavigationButtons.module.css';
import { usePathname } from 'next/navigation';
import DrinksLink from './links/DrinksLink';
import HomeLink from './links/HomeLink';
import MealsLink from './links/MealsLink';
import OrdersLink from './links/OrdersLink';

const NavigationButtons = () =>
{
  const pathname = usePathname();

  return (
    <>
      <div className={ styles.navigationButtons } >
        { pathname === "/meals" ? <DrinksLink /> : <MealsLink /> }
        <HomeLink />
        { pathname === "/orders" || pathname === "/ask" ? <DrinksLink /> : <OrdersLink /> }
      </div>
    </>
  );
}

export default NavigationButtons;
