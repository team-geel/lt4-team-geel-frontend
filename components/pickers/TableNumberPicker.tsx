import { useOrderDetails } from "@/context/orders";
import { ChangeEvent, MouseEvent, Suspense } from "react";
import styles from "@/styles/TableNumberPicker.module.css";
import ActionButton from "../buttons/ActionButton";
import { ButtonSizes, ButtonTypes } from "@/types/ButtonTypes";

const TablenumberPicker = () =>
{
  const { orderDetails, setOrderDetails } = useOrderDetails();

  function changeTableNumber ( event: ChangeEvent<HTMLSelectElement> )
  {
    const number = Number( event.target.value );

    if ( !Number.isNaN( number ) && number >= 1 && number <= 12 )
    {
      document.getElementById( "tableNumber" )?.classList.remove( styles.hidden );
      document.getElementById( "changeTableNumber" )?.classList.remove( styles.show )
      setOrderDetails( { ...orderDetails, tableNumber: number } );
    } else
    {
      alert(
        "Dit tafelnummer klopt niet. Voer alstublieft opnieuw een geldig tafelnummer in (1-12)"
      );
    }
  }

  function selectTableNumber ( event: MouseEvent<Element, MouseEvent> )
  {
    document.getElementById( "tableNumber" )?.classList.add( styles.hidden );
    document.getElementById( "changeTableNumber" )?.classList.add( styles.show )
  }

  return (
    <>
      <div className={ styles.tableNumber }>
        <ActionButton
          styleType={ ButtonTypes.TRANSPARENT }
          buttonSize={ ButtonSizes.SMALL }
          onClick={ ( event: any ) => { selectTableNumber( event ) } }
        >
          <h3 className={ styles.tableNumber__title }>
            <label id="tableNumber" className={ styles.tableNumber__label }>
              Huidig Tafelnummer: <Suspense fallback={ 0 }>{ orderDetails.tableNumber }</Suspense>
            </label>
            <div id="changeTableNumber" className={ styles.changeTableNumber }>
              <label className={ styles.changeTableNumber__label }>
                Voer alstublieft een nieuw tafelnummer in

                <select className={ styles.changeTableNumber__input } value={ orderDetails.tableNumber } onChange={ ( event ) => { changeTableNumber( event ) } }>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
              </label>
            </div>
          </h3>
        </ActionButton>
      </div>
    </>
  );
};

export default TablenumberPicker;
