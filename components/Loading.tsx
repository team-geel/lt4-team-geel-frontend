import { useEffect, useState } from "react";

const Loading = () =>
{
  const updateTime = 2000; // milliseconds
  const [ message, setMessage ] = useState( "Laden..." );

  useEffect( () =>
  {
    const interval = setTimeout( () =>
    {
      setMessage( "Geen items beschikbaar" );
    }, updateTime );

    return () => clearInterval( interval );

  }, [] )


  return ( <>
    <span>{ message }</span>
  </> );
}

export default Loading;
