import "react-slideshow-image/dist/styles.css";
import styles from "styles/Sliders.module.css";
import { Fade } from "react-slideshow-image";
import Image from 'next/image';
import { ImageSliderProps } from "@/types/ComponentTypes";

const deafultImages = [
  "/images/specials/cake.png",
  "/images/specials/coffee.png",
  "/images/specials/sandwich.png",
];

const deafultProperties = {
  duration: 3000,
  arrows: false,
};

const ImageSlider = ( { images = deafultImages, properties = deafultProperties }: ImageSliderProps ) =>
{
  return (
    <div className={ styles.container }>
      <Fade { ...properties }>
        {
          images.map( ( image: string, index: number ) =>
          {
            return (
              <div key={ `Image ${ index }` } className={ styles.eachslide }>
                <Image className={ styles.eachslide__image } src={ image } alt={ "slide-image-" + index } width={ 200 } height={ 200 } />
              </div>
            );
          } )
        }
      </Fade>
    </div>
  );
};

export default ImageSlider;


