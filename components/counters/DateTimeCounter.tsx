import { getDateNow } from "@/utils/globalFunctions";
import { Suspense, useEffect, useState } from "react";

const DateTimeCounter = () =>
{
  const updateTime = 1000; //in ms

  const [ date, setDate ] = useState( getDateNow() );

  useEffect( () =>
  {
    const interval = setTimeout( () =>
    {
      setDate( getDateNow( true ) );
    }, updateTime );

    return () => clearInterval( interval );

  }, [ date ] )

  return (
    <>
      <Suspense fallback={ "" }>
        <span>
          { date }
        </span>
      </Suspense>
    </>
  )
}

export default DateTimeCounter;
