import { useSelectedConsumables } from '@/context/selectedConsumables';
import styles from '@/styles/Counters.module.css';
import { ConsumableCounterProps } from '@/types/ComponentTypes';

const ConsumableCounter = ( ( { consumable }: ConsumableCounterProps ) =>
{
  const { getItemQuantity, decreaseItemQuantity, increaseItemQuantity } = useSelectedConsumables();

  return ( <>
    <div className={ styles.counter }>
      <button className={ styles.counter__minus } onClick={ () => decreaseItemQuantity( consumable ) }>
        -
      </button>
      <span className={ styles.counter__number }>
        { getItemQuantity( consumable ) }
      </span>
      <button className={ styles.counter__plus } onClick={ () => increaseItemQuantity( consumable ) }>
        +
      </button >
    </div >
  </> );
} );

export default ConsumableCounter;
