import { ButtonTypes } from "@/types/ButtonTypes";
import styles from "@/styles/Kitchen.module.css";
import OrderedItem from "./OrderedItem";
import Loading from "../Loading";
import ActionButton from "../buttons/ActionButton";
import { updateOrder } from "@/fetch/orders";
import { useKitchenOrders } from "@/context/kitchenOrders";
import { calcDateDifference, timestampToDate } from "@/utils/globalFunctions";
import { UpdateOrderType } from "@/types/OrderTypes";
import { OrderedItemsProps } from "@/types/KitchenOrderTypes";

const KitchenOrder = ( {
  id,
  tableNumber,
  orderComment,
  timestamp,
  status,
  orderList,
}: OrderedItemsProps ) =>
{
  const updateState: UpdateOrderType = { status: "Afgerond" }
  const { kitchenOrders, setkitchenOrders } = useKitchenOrders();

  function handleOrder ()
  {
    updateOrder( id, updateState );
    setkitchenOrders( kitchenOrders.filter( ( order ) =>
    {
      return order.id !== id;
    } ) )
  }

  function formattedTimestamp ()
  {
    const differenceDate = calcDateDifference( new Date(), timestampToDate( timestamp ) );

    let agoString = `${ differenceDate.getMinutes() } minuten geleden`;

    if ( differenceDate.getHours() === 1 )
    {
      agoString = `${ differenceDate.getHours() } uur`.concat( " en ", agoString )
    } else if ( differenceDate.getHours() > 1 )
    {
      agoString = `${ differenceDate.getHours() } uren`.concat( " en ", agoString )
    }

    if ( differenceDate.getMinutes() > 15 )
    {
      return <span className={ styles.danger }>{ agoString }</span>
    } else if ( differenceDate.getMinutes() > 30 || differenceDate.getHours() >= 1 )
    {
      return <span className={ styles.danger }>{ agoString }</span>
    }

    return agoString;
  }

  return ( <>
    <div className={ styles.order }>

      <div className={ styles.order__number }>
        <small className={ styles.timestamp }> { formattedTimestamp() }</small>
        <div className={ styles.orderNumber }>
          <p><b>Order</b></p>
          <p className={ styles.number }>{ id }</p>
        </div>
        <ActionButton onClick={ handleOrder } styleType={ ButtonTypes.DARK } >Afronden</ActionButton>
      </div>
      <div className={ styles.order__details }>
        <h4 className={ styles.tableNumber }>Tafelnummer: { tableNumber }</h4>
        {/* <small className={ styles.status }><b>Status:</b> { status }</small> */ }
        <p><b>Bericht klant:</b> </p>
        <p className={ styles.comment }>{ orderComment }</p>
      </div>
      <div className={ styles.order__list }>
        {
          ( orderList && orderList.length > 0
            ? orderList.map( ( orderedItem, index: number ) =>
            {
              return ( <OrderedItem key={ `OrderedItem ${ index } ` } { ...orderedItem } /> )
            } )
            : <Loading /> )
        }
      </div>
    </div>
  </> );
}

export default KitchenOrder;
