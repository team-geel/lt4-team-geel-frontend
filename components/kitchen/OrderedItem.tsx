import styles from "@/styles/Kitchen.module.css";
import { OrderedItemProps } from "@/types/KitchenOrderTypes";
import { ChangeEvent } from "react";

const OrderedItem = ( { name, quantity }: OrderedItemProps ) =>
{
  function handleCheckbox ( event: ChangeEvent<HTMLInputElement> )
  {
    const orderedItem = event.target.closest( `.${ styles.orderedItem }` );
    orderedItem?.classList.toggle( styles.checked );
  }

  return (
    <>
      <div className={ styles.orderedItem }>
        <h3 className={ styles.dishName }>{ name }</h3>
        <p>Hoeveelheid: { quantity }</p>
        <div className={ styles.checkbox }>
          <input
            type="checkbox"
            className={ styles.checkbox__input }
            onChange={ ( event ) =>
            {
              handleCheckbox( event )
            } }
          />
        </div>
      </div>
    </>
  );
};

export default OrderedItem;
