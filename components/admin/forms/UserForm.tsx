import React, { useEffect, useState } from "react";
import { postNewUser, updateUser } from "@/fetch/users";
import { NewUserType, Roles, UserFormProps } from "@/types/UserTypes";
import ActionButton from "@/components/buttons/ActionButton";
import From from "@/components/forms/From";
import FormGroup from "@/components/forms/FormGroup";
import { InputTypes } from "@/types/FormTypes";
import Input from "@/components/inputs/Input";
import Select from "@/components/inputs/Select";

const UserForm = ( { user }: UserFormProps ) =>
{
  const initialNewUser: NewUserType = {
    password: "",
    firstname: "",
    lastname: "",
    email: "",
    role: Roles.ADMIN,
  }
  const [ newUser, setNewUser ] = useState( initialNewUser );

  useEffect( () =>
  {
    if ( user )
    {
      setNewUser( { ...user, password: "" } )
    }
  }, [ user ] )

  const handleInputChange = ( event: React.ChangeEvent<HTMLInputElement> ) =>
  {
    const { name, value } = event.target;
    setNewUser( {
      ...newUser,
      [ name ]: value,
    } );
  };

  const userIsValid = () =>
  {
    if ( newUser.email && newUser.firstname && newUser.lastname && newUser.password && Object.values( Roles ).includes( newUser.role ) )
    {
      return true;
    }
    return false
  }

  const handleSubmit = ( event: React.FormEvent<HTMLFormElement> ) =>
  {
    event.preventDefault();
    let success = false;

    if ( userIsValid() )
    {
      if ( user )
      {
        success = updateUser( user.id, newUser );
      } else
      {
        success = postNewUser( newUser );
      }
    }

    if ( success )
    {
      window.alert( "Succes!" );
      setNewUser( {} as NewUserType );
    } else
    {
      window.alert( "Er ging iets mis. Controleer of alle velden zin ingevuld." );
    }
  };

  return (
    <From onSubmit={ handleSubmit }>
      <h2>
        {
          user ? (
            `Geselecteerd: ${ user.id } ${ user.email }`
          ) : (
            "Nieuwe gebruiker toevoegen"
          )
        }
      </h2>

      <FormGroup labelText="Voornaam:" name="firstname">
        <Input placeholder="voornaam" type={ InputTypes.TEXT } name="firstname" value={ newUser.firstname } onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Achternaam:" name="lastname">
        <Input placeholder="achternaam" type={ InputTypes.TEXT } name="lastname" value={ newUser.lastname } onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Email:" name="password">
        <Input placeholder="email" type={ InputTypes.EMAIL } name="email" value={ newUser.email } onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Wachtwoord:" name="password">
        <Input placeholder="wachtwoord" type={ InputTypes.PASSWORD } name="password" value={ newUser.password } onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Role:" name="roles">
        <Select name="roles" currentValue={ newUser.role } options={ Object.values( Roles ) } onChange={ ( e ) => setNewUser( { ...newUser, role: e.target.value as Roles } ) } />
      </FormGroup>
      <ActionButton type="submit">Submit</ActionButton>
    </From>
  );
};
export default UserForm;
