import React, { useEffect, useState } from "react";
import styles from "@/styles/Login.module.css";
import { postLogin } from "@/fetch/users";
import { useAuth } from "@/context/auth";
import { useRouter } from "next/navigation";
import ActionButton from "../../buttons/ActionButton";
import { ButtonTypes } from "@/types/ButtonTypes";
import { UserLoginType } from "@/types/UserTypes";
import Form from "@/components/forms/From";
import FormGroup from "@/components/forms/FormGroup";
import { InputTypes } from "@/types/FormTypes";
import Input from "@/components/inputs/Input";
const LoginForm = () =>
{
  const [ success, setSuccess ] = useState( true );
  const [ formData, setFormData ] = useState( {} as UserLoginType )
  const { auth, setAuth } = useAuth();
  const router = useRouter();

  const handleSubmit = async ( event: React.FormEvent<HTMLFormElement> ) =>
  {
    event.preventDefault();
    try
    {
      const result = await postLogin( formData );
      if ( result )
      {
        setAuth( { ...auth, token: result.token, role: result.role } );
        setSuccess( true );
      }
      setSuccess( false );
    } catch ( error )
    {
      setSuccess( false );
    }
  };
  const home = () =>
  {
    router.push( "/" );
  };

  useEffect( () =>
  {
    if ( auth.role === "ADMIN" )
    {
      router.push( "/admin" );
    } else if ( auth.role === "CHEF" )
    {
      router.push( "/kitchen" );
    } else if ( auth.role === "WAITER" )
    {
      router.push( "/requests" );
    }
  }, [ auth.role ] );

  return (
    <div className={ styles.container }>
      <h2 className={ styles.formTitle }>Inloggen</h2>
      <p className={ styles.text }>
        Deze pagina is bedoeld voor personeel van Lunchroom Schmaak om toegang
        te krijgen tot ons systeem.
      </p>
      <ActionButton styleType={ ButtonTypes.LINE } onClick={ home } >
        Home
      </ActionButton>
      <Form onSubmit={ handleSubmit }>
        <FormGroup labelText="E-mail:" name="username">
          <Input placeholder="gebruikersnaam" type={ InputTypes.TEXT } name="username" value={ formData.email } onChange={ ( event ) => setFormData( { ...formData, email: event.target.value } ) } />
        </FormGroup>

        <FormGroup labelText="Wachtwoord:" name="password">
          <Input placeholder="wachtwoord" type={ InputTypes.PASSWORD } name="password" value={ formData.password } onChange={ ( event ) => setFormData( { ...formData, password: event.target.value } ) } />
        </FormGroup>

        <small>
          { !success && (
            <div className={ styles.error }>
              Onjuiste gegevens, probeer het opnieuw.
            </div>
          ) }
          In het geval je je login gegevens kwijt bent neem dan contact op met
          de leidinggevende.
        </small>
        <ActionButton className={ styles.formButton } type="submit">
          Inloggen
        </ActionButton>
      </Form>
    </div>
  );
};

export default LoginForm;
