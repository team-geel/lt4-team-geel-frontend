import React, { useState, useEffect } from "react";
import { ConsumableFormProps, NewConsumableItemType } from "@/types/ConsumableTypes";
import { postConsumable, putConsumable } from "@/fetch/consumables";
import styles from "@/styles/EditConsumableForm.module.css";
import FormGroup from "@/components/forms/FormGroup";
import Input from "@/components/inputs/Input";
import { InputTypes } from "@/types/FormTypes";
import Select from "@/components/inputs/Select";
import { MenuTypes } from "@/types/SelectedMenuTypes";
import ActionButton from "@/components/buttons/ActionButton";
import Form from "@/components/forms/From";

const ConsumableForm = ( { consumable }: ConsumableFormProps ) =>
{
  const initialConsumable: NewConsumableItemType = {
    name: "",
    price: 0,
    image: "https://placehold.jp/a03232/ffffff/80x80.png",
    type: MenuTypes.MEALS,
    category: "",
    description: "",
  }
  const [ newConsumable, setNewConsumable ] = useState( initialConsumable );

  useEffect( () =>
  {
    if ( consumable )
    {
      setNewConsumable( { ...consumable, ...newConsumable } )
    }
  }, [] )


  const handleInputChange = ( event: React.ChangeEvent<HTMLInputElement> ) =>
  {
    const { name, value } = event.target;
    setNewConsumable( {
      ...newConsumable,
      [ name ]: value,
    } );
  };

  const consumableIsValid = () =>
  {
    if ( newConsumable.category && newConsumable.image && newConsumable.name && newConsumable.price && Object.values( MenuTypes ).includes( newConsumable.type ) )
    {
      return true;
    }
    return false
  }

  const handleFormSubmit = ( event: React.FormEvent ) =>
  {
    event.preventDefault();
    let success = false;

    if ( consumableIsValid() )
      if ( consumable )
      {
        success = putConsumable( newConsumable, consumable.id );
      } else
      {
        success = postConsumable( newConsumable );
      }

    if ( success )
    {
      window.alert( "Succes!" );
      setNewConsumable( initialConsumable );
      console.log( newConsumable )
    } else
    {
      window.alert( "Er ging iets mis. Controleer of alle velden zin ingevuld." );
    }
  };

  return (
    <Form onSubmit={ handleFormSubmit }>
      <h2 className={ styles.message }>
        {
          consumable ? (
            `Geselecteerd: ${ consumable.id } ${ consumable.name }`
          ) : (
            "Nieuw product toevoegen"
          )
        }
      </h2>

      <FormGroup labelText="Name:" name="name">
        <Input placeholder="naam" value={ newConsumable.name } type={ InputTypes.TEXT } name="name" onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Prijs:" name="price">
        <Input placeholder="prijs" value={ newConsumable.price } type={ InputTypes.NUMBER } step="0.01" name="price" onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Image:" name="image">
        <Input placeholder="image" value={ newConsumable.image } type={ InputTypes.TEXT } accept="image/png, image/jpeg, image/jpg" name="image" onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Type:" name="type">
        <Select name="type" currentValue={ newConsumable.type } options={ Object.values( MenuTypes ) } onChange={ ( e ) => setNewConsumable( { ...newConsumable, type: e.target.value as MenuTypes } ) } />
      </FormGroup>

      <FormGroup labelText="Category:" name="category">
        <Input placeholder="category" value={ newConsumable.category } type={ InputTypes.TEXT } name="category" onChange={ handleInputChange } />
      </FormGroup>

      <FormGroup labelText="Beschrijving:" name="description">
        <Input placeholder="beschrijving" value={ newConsumable.description } type={ InputTypes.TEXT } name="description" onChange={ handleInputChange } />
      </FormGroup>

      <ActionButton type="submit">
        Opslaan
      </ActionButton>
    </Form>
  );
};

export default ConsumableForm;
