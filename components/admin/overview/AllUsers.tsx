import { getUsers } from "@/fetch/users";
import { UserType } from "@/types/UserTypes";
import { useEffect, useState } from "react";
import styles from "@/styles/Users.module.css";
import ActionButton from "@/components/buttons/ActionButton";
import UserForm from "../forms/UserForm";
import { useAuth } from "@/context/auth";
import { hasValues } from "@/utils/globalFunctions";

const AllUsers = () =>
{
  const { auth } = useAuth();
  const [ users, setUsers ] = useState<UserType[]>( [] );
  const [ selectedUser, setSelectedUser ] = useState( {} as UserType );

  const handleEdit = ( user: UserType ) =>
  {
    setSelectedUser( user );
    document.getElementById( "editForm" )?.classList.add( "show" );
    document.getElementById( "table" )?.classList.add( "hidden" )
  };

  useEffect( () =>
  {
    const fetchData = async () =>
    {
      const result = await getUsers( auth );
      setUsers( result );
    }

    fetchData();

  }, [] )

  return (
    <>
      {
        hasValues( selectedUser ) ? (
          <UserForm user={ selectedUser } /> ) : (
          <div className={ styles.table }>
            <div className={ styles.table__head }>
              <span>ID</span>
              <span>Naam</span>
              <span>Email</span>
              <span>Rol</span>
            </div>
            <div className={ styles.table__body }>
              { users.map( ( user ) => (
                <div key={ user.id } className={ styles.tableItem }>
                  <ActionButton onClick={ () => handleEdit( user ) }>
                    Aanpassen
                  </ActionButton>
                  <span>{ user.id }</span>
                  <span>{ user.firstname }</span>
                  <span>{ user.lastname }</span>
                  <span>{ user.email }</span>
                  <span>{ user.role }</span>
                </div>
              ) ) }
            </div>
          </div>
        )
      }
    </>
  );
}

export default AllUsers;
