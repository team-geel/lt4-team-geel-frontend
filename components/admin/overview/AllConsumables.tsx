import Loading from "@/components/Loading";
import { deleteConsumable, getConsumables } from "@/fetch/consumables";
import { ConsumableItemType } from "@/types/ConsumableTypes";
import { useEffect, useState } from "react";
import styles from "@/styles/Consumables.module.css";
import ActionButton from "@/components/buttons/ActionButton";
import Image from 'next/image';
import ConsumableForm from "../forms/ConsumableForm";
import { hasValues } from "@/utils/globalFunctions";
import { ButtonSizes, ButtonTypes } from "@/types/ButtonTypes";

const AllConsumables = () =>
{
  const [ consumables, setConsumables ] = useState<ConsumableItemType[]>( [] );
  const [ isDeleted, setIsDeleted ] = useState( false );
  const [ selectedConsumable, setSelectedConsumable ] = useState( {} as ConsumableItemType );

  const handleEdit = ( consumable: ConsumableItemType ) =>
  {
    setSelectedConsumable( consumable );
  };

  const handleDelete = ( consumable: ConsumableItemType ) =>
  {
    if (
      window.confirm(
        `Weet je zeker dat je ${ consumable.id } ${ consumable.name } van het menu wilt verwijderen??`
      )
    )
    {
      deleteConsumable( consumable.id )
      setIsDeleted( true )
    }
  }

  useEffect( () =>
  {
    const fetchData = async () =>
    {
      const result = await getConsumables();
      setConsumables( result );
    }

    fetchData();

  }, [ isDeleted ] )

  return (
    <>
      {
        hasValues( selectedConsumable ) ? (
          <ConsumableForm consumable={ selectedConsumable } /> )
          : (
            <div className={ styles.table }>
              <div className={ styles.table__head }>
                <span>ID</span>
                <span>Naam</span>
                <span>Prijs</span>
                <span>Image</span>
                <span>Type</span>
                <span>Category</span>
                <span>Beschrijving</span>
              </div>
              <div className={ styles.table__body }>
                { consumables.map( ( consumable ) => (
                  <div key={ consumable.id } className={ styles.tableItem }>
                    <ActionButton buttonSize={ ButtonSizes.SMALL } styleType={ ButtonTypes.DARK } onClick={ () => handleEdit( consumable ) }>
                      Aanpassen
                    </ActionButton>
                    <span>{ consumable.id }</span>
                    <span>{ consumable.name }</span>
                    <span>{ consumable.price }</span>
                    <span>
                      <Image
                        src={ consumable.image }
                        alt={ consumable.name + " image" }
                        className={ styles.consumable__image }
                        width={ 80 }
                        height={ 80 }
                      />
                    </span>
                    <span>{ consumable.type }</span>
                    <span>{ consumable.category }</span>
                    <span>{ consumable.description }</span>
                    <ActionButton buttonSize={ ButtonSizes.SMALL } styleType={ ButtonTypes.DARK } onClick={ () => handleDelete( consumable ) }>
                      Verwijderen
                    </ActionButton>
                  </div>
                ) ) }
              </div>
            </div>
          )
      }
    </>
  )
}

export default AllConsumables;
