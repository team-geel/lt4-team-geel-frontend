import React, { useState } from "react";
import styles from "@/styles/Tabs.module.css";
import ActionButton from "../../buttons/ActionButton";
import { ButtonSizes, ButtonTypes } from "@/types/ButtonTypes";
import ConsumableForm from "../forms/ConsumableForm";
import AllConsumables from "../overview/AllConsumables";
import UserForm from "../forms/UserForm";
import AllUsers from "../overview/AllUsers";

const Tabs = () =>
{
  const [ activeTab, setActiveTab ] = useState( "addProducts" );

  const handleTabChange = ( tab: string ) =>
  {
    setActiveTab( tab );
  };

  return (
    <div className={ styles.container }>
      <div className={ styles.tabs }>
        <ActionButton styleType={ ButtonTypes.DARK } buttonSize={ ButtonSizes.SMALL } active={ activeTab === "addProducts" }
          onClick={ () => handleTabChange( "addProducts" ) }
        >
          Product toevoegen
        </ActionButton>
        <ActionButton styleType={ ButtonTypes.DARK } buttonSize={ ButtonSizes.SMALL } active={ activeTab === "editProducts" }
          onClick={ () => handleTabChange( "editProducts" ) }
        >
          Producten wijzigen
        </ActionButton>
        <ActionButton styleType={ ButtonTypes.DARK } buttonSize={ ButtonSizes.SMALL } active={ activeTab === "addUsers" }
          onClick={ () => handleTabChange( "addUsers" ) }
        >
          Gebruikers toevoegen
        </ActionButton>
        <ActionButton styleType={ ButtonTypes.DARK } buttonSize={ ButtonSizes.SMALL } active={ activeTab === "editUsers" }
          onClick={ () => handleTabChange( "editUsers" ) }
        >
          Gebruikers wijzigen
        </ActionButton>
      </div>
      <div className={ styles.tabContent }>
        { activeTab === "addProducts" && <ConsumableForm /> }
        { activeTab === "editProducts" && <AllConsumables /> }
        { activeTab === "addUsers" && <UserForm /> }
        { activeTab === "editUsers" && <AllUsers /> }
      </div>
    </div>
  );
};

export default Tabs;
