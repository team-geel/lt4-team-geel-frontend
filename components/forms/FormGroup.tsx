import styles from '@/styles/Forms.module.css';
import { FormGroupProps } from '@/types/FormTypes';
import Input from '../inputs/Input';

const FormGroup = ( { name, children, labelText }: FormGroupProps ) =>
{
  return (
    <div className={ styles.formGroup }>
      <label htmlFor={ name }>{ labelText }</label>
      { children }
    </div>
  )
}

export default FormGroup;
