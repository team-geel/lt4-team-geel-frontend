import styles from '@/styles/Forms.module.css';
import { FormProps } from '@/types/FormTypes';

const Form = ( { children, onSubmit }: FormProps ) =>
{
  return (
    <>
      <form className={ styles.form } onSubmit={ onSubmit }>
        { children }
      </form>
    </>
  )
}

export default Form;
