async function request<T> (
  url: string,
  config: RequestInit = {},
): Promise<T | Error>
{
  console.log( url ); console.log( config );
  const response = await fetch( url, config );
  console.log( response );
  return await response.json();
}

export function getRequest<T> (
  url: string,
  config: RequestInit = {},
): Promise<T | Error>
{
  return request<T>( url, config );
}

export function postRequest<D, T> (
  url: string,
  dataToSend: D,
  config: RequestInit = {},
): Promise<T | Error>
{
  const postConfig = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify( dataToSend ),
    ...config,
  }
  return request<T>( url, postConfig );
}

export function putRequest<D, T> (
  url: string,
  dataToSend: D,
  config: RequestInit = {},
): Promise<T | Error>
{
  const putConfig = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify( dataToSend ),
    ...config,
  }
  return request<T>( url, putConfig );
}

export function deleteRequest<T> (
  url: string,
  config: RequestInit = {},
): Promise<T | Error>
{
  const deleteConfig = {
    method: "DELETE",
    ...config,
  }
  return request<T>( url, deleteConfig );
}

export async function tryRequest<T> ( request: Promise<T | Error> ): Promise<T>
{
  try
  {
    return await request
      .then( ( data ) =>
      {
        return data as T
      } )
      .catch( ( error: Error ) =>
      {
        //Todo: better error handling
        console.log( "Error on the server occurred. Reason: ", error );
        return [] as T;
      } );
  }
  catch ( error: any )
  {
    //Todo: better error handling
    console.log( "Error while connecting to server occurred. Reason: ", error );
    return [] as T;
  }
}
