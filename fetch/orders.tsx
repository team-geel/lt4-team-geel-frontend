import { OrderedItemType } from "@/types/KitchenOrderTypes";
import { OrderType, UpdateOrderType } from "@/types/OrderTypes";
import { postRequest, putRequest, getRequest, tryRequest } from "./requests";

export function getOrderedOrders ()
{
  const request = getRequest<OrderedItemType[]>(
    "http://localhost:8080/api/order"
  );

  return tryRequest( request );
}

export function postOrder ( order: OrderType )
{
  const request = postRequest<OrderType, null>(
    "http://localhost:8080/api/order",
    order
  );

  tryRequest( request );

  return true;
}

export function updateOrder ( id: number, update: UpdateOrderType )
{
  const request = putRequest<UpdateOrderType, null>(
    `http://localhost:8080/api/order/update/${ id }`,
    update
  );

  tryRequest( request );

  return true;
}
