import { ConsumableItemType, NewConsumableItemType } from "@/types/ConsumableTypes";
import { getRequest, postRequest, tryRequest, putRequest, deleteRequest } from "./requests";

export function getConsumables ()
{
  const request = getRequest<ConsumableItemType[]>(
    "http://localhost:8080/api/consumable"
  );

  return tryRequest( request );
}

export function postConsumable ( data: NewConsumableItemType )
{
  const request = postRequest<NewConsumableItemType, null>(
    "http://localhost:8080/api/consumable",
    data
  );

  tryRequest( request );

  return true;
}

export function putConsumable ( data: NewConsumableItemType, id: number )
{
  const request = putRequest<NewConsumableItemType, null>(
    `http://localhost:8080/api/consumable/${ id }`,
    data
  );
  tryRequest( request );
  return true;
}

export function deleteConsumable ( id: number )
{
  const request = deleteRequest<null>(
    `http://localhost:8080/api/consumable/${ id }`
  );
  tryRequest( request );
  return true;
}
