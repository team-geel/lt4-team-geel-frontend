import { AuthType, NewUserType, UserLoginType, UserType } from "@/types/UserTypes";
import { postRequest, putRequest, getRequest, tryRequest } from "./requests";

export function getUsers ( auth: AuthType )
{
  if ( !auth )
  {
    console.log( "No token found" );
    return []
  }

  const request = getRequest<UserType[]>(
    "http://localhost:8080/api/auth/users/all",
    {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${ auth.token }`,
      }
    }
  );

  return tryRequest( request );
}

export function postLogin ( userLogin: UserLoginType )
{
  const request = postRequest<UserLoginType, AuthType>(
    "http://localhost:8080/api/auth/authenticate",
    userLogin
  );

  return tryRequest( request );
}

export function postNewUser ( newUser: NewUserType )
{
  const request = postRequest<NewUserType, null>(
    "http://localhost:8080/api/auth/register",
    newUser
  );

  tryRequest( request );

  return true;
}

export function updateUser ( id: number, updatedUser: NewUserType )
{
  const request = putRequest<NewUserType, null>(
    "http://localhost:8080/api/auth/update/" + id,
    updatedUser
  );

  tryRequest( request );

  return true;
}


