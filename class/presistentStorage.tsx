export interface PersistentStorage
{
  getItem ( key: string ): any | null | undefined;
  setItem ( key: string, value: any ): void;
}

export class LocalStorage<T> implements PersistentStorage
{
  getItem ( key: string ): T | null | undefined
  {
    if ( typeof window === 'undefined' )
    {
      return null;
    }

    const jsonItem = localStorage.getItem( key );

    if ( jsonItem === null ) { return undefined }
    if ( jsonItem === "null" ) { return null }
    if ( jsonItem === "undefined" ) { return undefined }

    try
    {
      return JSON.parse( jsonItem ) as T;
    } catch
    {
      console.log( "Incognito" );
      //TODO: error handling
    }

    return jsonItem as T;
  }

  setItem ( key: string, value: T ): void
  {
    if ( typeof window === 'undefined' )
    {
      return;
    }

    if ( value === undefined )
    {
      localStorage.removeItem( key );
    } else
    {
      localStorage.setItem( key, JSON.stringify( value ) );
    }
  }
}

