import { useEffect } from "react";
import { useRouter } from "next/router";
import { useAuth } from "@/context/auth";

const useAuthorisation = () =>
{
  const router = useRouter();
  const { auth } = useAuth();

  useEffect( () =>
  {
    if ( typeof auth === "undefined" )
    {
      router.push( "/login" );
    }
  }, [] );
};

export default useAuthorisation;
