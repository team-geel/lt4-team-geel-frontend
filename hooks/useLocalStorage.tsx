import { LocalStorage } from "@/class/presistentStorage";
import { useEffect, useState } from "react";
import { isObject } from "@/utils/globalFunctions";

export function useLocalStorage<T> ( key: string, fallbackState?: T | ( () => T ) )
{

  const persistentStorage = new LocalStorage<T>();
  const [ state, setState ] = useState<T>( () =>
  {
    const storedItem = persistentStorage.getItem( key );

    if ( storedItem === null || storedItem === undefined )
    {
      return fallbackState as T;
    }

    if ( isObject( fallbackState ) && isObject( storedItem ) )
    {
      return {
        ...fallbackState,
        ...storedItem
      } as T;
    }

    if ( typeof fallbackState === 'function' )
    {
      return ( fallbackState as T );
    }

    return storedItem!;
  } );

  useEffect( () =>
  {
    persistentStorage.setItem( key, state );
  }, [ state, key ] );


  return [ state, setState ] as [ typeof state, typeof setState ];
}
