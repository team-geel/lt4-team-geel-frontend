import { OrderContextProvider } from "@/context/orders";
import { ConsumableMenuProvider } from "@/context/selectedMenu";
import "@/styles/reset.css";
import "@/styles/globals.css";
import type { AppProps } from "next/app";
import MobileLayout from "./layout/MobileLayout";
import { usePathname } from "next/navigation";
import DesktopLayout from "./layout/DesktopLayout";
import { KitchenProvider } from "@/context/kitchenOrders";
import { SelectedConsumablesContextProvider } from "@/context/selectedConsumables";
import { ServiceRequestContextProdiver } from "@/context/serviceRequest";
import { AuthContextProvider } from "@/context/auth";
import useAuthorisation from "@/hooks/useAuthorisation";

export default function App ( { Component, pageProps }: AppProps )
{
  const pathname = usePathname();

  if (
    pathname === "/kitchen" )
  {
    useAuthorisation();
    return (
      <AuthContextProvider>
        <KitchenProvider>
          <DesktopLayout>
            <Component { ...pageProps } />
          </DesktopLayout>
        </KitchenProvider>
      </AuthContextProvider>
    );
  }
  if (
    pathname === "/admin" ||
    pathname === "/login" ||
    pathname === "/requests"
  )
  {
    useAuthorisation();
    return (
      <AuthContextProvider>
        <DesktopLayout>
          <Component { ...pageProps } />
        </DesktopLayout>
      </AuthContextProvider>
    );
  }

  return (
    <MobileLayout>
      <OrderContextProvider>
        <ServiceRequestContextProdiver>
          <SelectedConsumablesContextProvider>
            <ConsumableMenuProvider>
              <Component { ...pageProps } />
            </ConsumableMenuProvider>
          </SelectedConsumablesContextProvider>
        </ServiceRequestContextProdiver>
      </OrderContextProvider>
    </MobileLayout>
  );
}
