import LoginForm from "@/components/admin/forms/LoginForm";
import React from "react";

const Login = () =>
{
  return (
    <LoginForm />
  );
};

export default Login;
