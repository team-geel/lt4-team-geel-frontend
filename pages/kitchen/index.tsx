import styles from "@/styles/Kitchen.module.css";
import KitchenOrder from "@/components/kitchen/KitchenOrder";
import { useKitchenOrders } from "@/context/kitchenOrders";
import { OrderedItemType } from "@/types/KitchenOrderTypes";
import Loading from "@/components/Loading";

const Kitchen = () =>
{
  const { kitchenOrders } = useKitchenOrders();

  return (
    <>
      {
        kitchenOrders ? (
          <div className={ styles.orders }>
            { kitchenOrders.map( ( order: OrderedItemType, index: number ) =>
            {
              return <KitchenOrder key={ `KitchenOrder ${ index }` } { ...order } />;
            } ) }
          </div>
        ) : <Loading />
      }
    </>
  );
};

export default Kitchen;
