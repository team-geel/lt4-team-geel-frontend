import TablenumberPicker from '@/components/pickers/TableNumberPicker';
import PageNavigation from '@/components/navigation/PageNavigation';
import ImageSlider from '@/components/sliders/ImageSlider';

export default function Home ()
{

  return (
    <>
      <TablenumberPicker />
      <PageNavigation />
      <ImageSlider />
    </>
  )
}
