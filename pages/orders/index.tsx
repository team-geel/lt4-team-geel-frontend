import SelectedConsumables from "@/components/consumables/SelectedConsumables";
import NavigationButtons from "@/components/navigation/NavigationButtons";
import CommentForm from "@/components/inputs/CommentForm";

const Orders = () => {
  return (
    <>
      <SelectedConsumables />
      <CommentForm commentType="orderComment" />
      <NavigationButtons />
    </>
  );
};

export default Orders;
