import CommentForm from "@/components/inputs/CommentForm";
import NavigationButtons from "@/components/navigation/NavigationButtons";
import styles from "@/styles/CommentForm.module.css";

const Ask = () => {
  return (
    <>
      <CommentForm commentType="serviceRequest" />
      <p className={styles.formText}>
        We zullen u zo snel mogelijk helpen, duurt het lang? Vraag gerust een
        personeelslid om hulp.
      </p>
      <NavigationButtons />
    </>
  );
};

export default Ask;
