import Head from "next/head";
import styles from "@/styles/Layout.module.css";
import Footer from "@/components/navigation/Footer";
import { DesktopLayoutProps } from "@/types/LayoutTypes";
import DesktopNavbar from "@/components/navigation/navBars/DesktopNavbar";
import SecondaryNavbar from "@/components/navigation/navBars/SecondaryNavbar";
import { useAuth } from "@/context/auth";

const Layout = ( { children }: DesktopLayoutProps ) =>
{
  const user = useAuth();

  return (
    <>
      <Head>
        <title>Lunchroom Schmaak Kitchen</title>
        <meta name="description" content="Eetcaffé" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <DesktopNavbar />
      <main className={ styles.desktopMain }>{ children }</main>
      <Footer />
    </>
  );
};

export default Layout;
