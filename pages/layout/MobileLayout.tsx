import Head from "next/head";
import styles from "@/styles/Layout.module.css";
import Footer from "@/components/navigation/Footer";
import { MobileLayoutProps } from "@/types/LayoutTypes";
import MobileNavbar from "@/components/navigation/navBars/MobileNavbar";

const MobileLayout = ({ children }: MobileLayoutProps) => {
  return (
    <>
      <Head>
        <title>Lunchroom Schmaak</title>
        <meta name="description" content="Eetcaffé" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.mobileScreen}>
        <MobileNavbar />
        <main className={styles.mobileMain}>{children}</main>
        <Footer />
      </div>
    </>
  );
};

export default MobileLayout;
