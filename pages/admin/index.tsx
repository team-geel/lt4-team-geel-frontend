import styles from "@/styles/Admin.module.css";

import TabNavigator from "@/components/admin/tabs/TabNavigator";

const index = () =>
{
  return (
    <div className={ styles.tabs }>
      <TabNavigator />
    </div>
  );
};

export default index;
