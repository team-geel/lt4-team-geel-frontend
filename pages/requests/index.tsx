import Loading from "@/components/Loading";
import { Suspense, useEffect, useState } from "react";
import styles from '@/styles/Orders.module.css';
import { getOrderedOrders } from "@/fetch/orders";
import { OrderedItemType } from "@/types/KitchenOrderTypes";

const Requests = () =>
{
  const initialConsumables: OrderedItemType[] = [];
  const [ consumables, setconsumables ] = useState<OrderedItemType[]>( initialConsumables );

  useEffect( () =>
  {
    const fetchData = async () =>
    {
      const result = await getOrderedOrders();
      setconsumables( result );
    }

    fetchData();

  }, [] )

  function filterConsumables ( consumables: OrderedItemType[] )
  {
    return consumables.filter( ( consumable ) =>
    {
      return consumable.status === 'Afgerond';
    } );
  }

  if ( !consumables )
  {
    return <p>Geen items gevonden</p>;
  };

  if ( consumables.length <= 0 )
  {
    return <Loading />;
  }

  return ( <>
    <Suspense fallback={ undefined }>
      <div className={ styles.container } >
        <div className={ styles.OrdersDone }>
          {
            filterConsumables( consumables ).map( ( consumable: OrderedItemType, index: number ) =>
            {
              return (
                <div key={ index } className={ styles.OrdersDoneItem }>
                  <b>Bij tafelnummer { consumable.tableNumber } is dit besteld en afgerond:</b>
                  <ul className={ styles.List }>
                    { consumable.orderList.map( ( item ) =>
                    {
                      return (
                        <li key={ item.name } className={ styles.ListItem }>
                          { item.name } is { item.quantity } aantal keer besteld
                        </li>
                      )
                    } ) }
                  </ul>
                </div>
              )
            } )
          }
        </div>
      </div>
    </Suspense>
  </> );
};

export default Requests;
