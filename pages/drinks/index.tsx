import Consumables from "@/components/consumables/Consumables";
import { MenuTypes } from "@/types/SelectedMenuTypes";

const Drinks = () =>
{
  return ( <>
    <Consumables forType={ MenuTypes.DRINKS } />
  </> );
}

export default Drinks;
