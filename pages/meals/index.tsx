import Consumables from "@/components/consumables/Consumables";
import { MenuTypes } from "@/types/SelectedMenuTypes";

const Meals = () =>
{
  return ( <>
    <Consumables forType={ MenuTypes.MEALS } />
  </> );
}

export default Meals;
